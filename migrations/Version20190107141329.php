<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190107141329 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE relation_users_roles (phpauth_users_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_35C832941838B754 (phpauth_users_id), INDEX IDX_35C83294D60322AC (role_id), PRIMARY KEY(phpauth_users_id, role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phpauth_users_group (phpauth_users_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_9CF619371838B754 (phpauth_users_id), INDEX IDX_9CF61937FE54D947 (group_id), PRIMARY KEY(phpauth_users_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phpauth_users_pad (phpauth_users_id INT NOT NULL, pad_id INT NOT NULL, INDEX IDX_9A438B2C1838B754 (phpauth_users_id), INDEX IDX_9A438B2C33F68EE9 (pad_id), PRIMARY KEY(phpauth_users_id, pad_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_wrapper_groups (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, group_id VARCHAR(18) NOT NULL, created DATETIME NOT NULL, INDEX IDX_381BD2537E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation_authors_groups (group_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_45AF876DFE54D947 (group_id), UNIQUE INDEX UNIQ_45AF876DA76ED395 (user_id), PRIMARY KEY(group_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_wrapper_pads (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, is_password_protected TINYINT(1) NOT NULL, name VARCHAR(200) NOT NULL, pad_id VARCHAR(219) NOT NULL, is_private TINYINT(1) NOT NULL, INDEX IDX_A0C31C137E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation_authors_pads (pad_id INT NOT NULL, phpauth_users_id INT NOT NULL, INDEX IDX_D36BDEB33F68EE9 (pad_id), INDEX IDX_D36BDEB1838B754 (phpauth_users_id), PRIMARY KEY(pad_id, phpauth_users_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE relation_pads_groups (pad_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_7863731D33F68EE9 (pad_id), INDEX IDX_7863731DFE54D947 (group_id), PRIMARY KEY(pad_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_wrapper_roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE relation_users_roles ADD CONSTRAINT FK_35C832941838B754 FOREIGN KEY (phpauth_users_id) REFERENCES phpauth_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE relation_users_roles ADD CONSTRAINT FK_35C83294D60322AC FOREIGN KEY (role_id) REFERENCES user_wrapper_roles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE phpauth_users_group ADD CONSTRAINT FK_9CF619371838B754 FOREIGN KEY (phpauth_users_id) REFERENCES phpauth_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE phpauth_users_group ADD CONSTRAINT FK_9CF61937FE54D947 FOREIGN KEY (group_id) REFERENCES user_wrapper_groups (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE phpauth_users_pad ADD CONSTRAINT FK_9A438B2C1838B754 FOREIGN KEY (phpauth_users_id) REFERENCES phpauth_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE phpauth_users_pad ADD CONSTRAINT FK_9A438B2C33F68EE9 FOREIGN KEY (pad_id) REFERENCES user_wrapper_pads (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_wrapper_groups ADD CONSTRAINT FK_381BD2537E3C61F9 FOREIGN KEY (owner_id) REFERENCES phpauth_users (id)');
        $this->addSql('ALTER TABLE relation_authors_groups ADD CONSTRAINT FK_45AF876DFE54D947 FOREIGN KEY (group_id) REFERENCES user_wrapper_groups (id)');
        $this->addSql('ALTER TABLE relation_authors_groups ADD CONSTRAINT FK_45AF876DA76ED395 FOREIGN KEY (user_id) REFERENCES phpauth_users (id)');
        $this->addSql('ALTER TABLE user_wrapper_pads ADD CONSTRAINT FK_A0C31C137E3C61F9 FOREIGN KEY (owner_id) REFERENCES phpauth_users (id)');
        $this->addSql('ALTER TABLE relation_authors_pads ADD CONSTRAINT FK_D36BDEB33F68EE9 FOREIGN KEY (pad_id) REFERENCES user_wrapper_pads (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE relation_authors_pads ADD CONSTRAINT FK_D36BDEB1838B754 FOREIGN KEY (phpauth_users_id) REFERENCES phpauth_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE relation_pads_groups ADD CONSTRAINT FK_7863731D33F68EE9 FOREIGN KEY (pad_id) REFERENCES user_wrapper_pads (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE relation_pads_groups ADD CONSTRAINT FK_7863731DFE54D947 FOREIGN KEY (group_id) REFERENCES user_wrapper_groups (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE phpauth_users ADD username VARCHAR(100) DEFAULT NULL, ADD author_id VARCHAR(18) DEFAULT NULL, CHANGE email email VARCHAR(100) DEFAULT NULL, CHANGE password password VARCHAR(255) DEFAULT NULL, CHANGE isactive isactive TINYINT(1) NOT NULL, CHANGE dt dt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE phpauth_config CHANGE setting setting VARCHAR(100) NOT NULL, CHANGE value value VARCHAR(100) DEFAULT NULL, ADD PRIMARY KEY (setting)');
        $this->addSql('ALTER TABLE phpauth_emails_banned CHANGE domain domain VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE phpauth_users_group DROP FOREIGN KEY FK_9CF61937FE54D947');
        $this->addSql('ALTER TABLE relation_authors_groups DROP FOREIGN KEY FK_45AF876DFE54D947');
        $this->addSql('ALTER TABLE relation_pads_groups DROP FOREIGN KEY FK_7863731DFE54D947');
        $this->addSql('ALTER TABLE phpauth_users_pad DROP FOREIGN KEY FK_9A438B2C33F68EE9');
        $this->addSql('ALTER TABLE relation_authors_pads DROP FOREIGN KEY FK_D36BDEB33F68EE9');
        $this->addSql('ALTER TABLE relation_pads_groups DROP FOREIGN KEY FK_7863731D33F68EE9');
        $this->addSql('ALTER TABLE relation_users_roles DROP FOREIGN KEY FK_35C83294D60322AC');
        $this->addSql('DROP TABLE relation_users_roles');
        $this->addSql('DROP TABLE phpauth_users_group');
        $this->addSql('DROP TABLE phpauth_users_pad');
        $this->addSql('DROP TABLE user_wrapper_groups');
        $this->addSql('DROP TABLE relation_authors_groups');
        $this->addSql('DROP TABLE user_wrapper_pads');
        $this->addSql('DROP TABLE relation_authors_pads');
        $this->addSql('DROP TABLE relation_pads_groups');
        $this->addSql('DROP TABLE user_wrapper_roles');
        $this->addSql('ALTER TABLE phpauth_config MODIFY setting VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE phpauth_config DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE phpauth_config CHANGE setting setting VARCHAR(100) NOT NULL COLLATE latin1_swedish_ci, CHANGE value value VARCHAR(100) DEFAULT \'NULL\' COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE phpauth_emails_banned CHANGE domain domain VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE phpauth_users DROP username, DROP author_id, CHANGE email email VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_general_ci, CHANGE password password VARCHAR(255) DEFAULT \'NULL\' COLLATE latin1_general_ci, CHANGE isactive isactive TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE dt dt DATETIME DEFAULT \'current_timestamp()\' NOT NULL');
    }
}
