<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190107220645 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE phpauth_users CHANGE email email VARCHAR(100) DEFAULT NULL, CHANGE password password VARCHAR(255) DEFAULT NULL, CHANGE dt dt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE username username VARCHAR(100) DEFAULT NULL, CHANGE author_id author_id VARCHAR(18) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_wrapper_groups CHANGE owner_id owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE phpauth_config CHANGE setting setting VARCHAR(100) NOT NULL, CHANGE value value VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE phpauth_emails_banned CHANGE domain domain VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_wrapper_pads CHANGE owner_id owner_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE phpauth_config CHANGE setting setting VARCHAR(100) NOT NULL COLLATE latin1_swedish_ci, CHANGE value value VARCHAR(100) DEFAULT \'NULL\' COLLATE latin1_swedish_ci');
        $this->addSql('ALTER TABLE phpauth_emails_banned CHANGE domain domain VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE phpauth_users CHANGE email email VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_general_ci, CHANGE username username VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_general_ci, CHANGE password password VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_general_ci, CHANGE author_id author_id VARCHAR(18) DEFAULT \'NULL\' COLLATE utf8_general_ci, CHANGE dt dt DATETIME DEFAULT \'current_timestamp()\' NOT NULL');
        $this->addSql('ALTER TABLE user_wrapper_groups CHANGE owner_id owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_wrapper_pads CHANGE owner_id owner_id INT DEFAULT NULL');
    }
}
