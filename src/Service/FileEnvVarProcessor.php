<?php

namespace App\Service;

use \Symfony\Component\DependencyInjection\EnvVarProcessorInterface;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\DependencyInjection\Exception\EnvNotFoundException;
use \Symfony\Component\DependencyInjection\Exception\RuntimeException;



class FileEnvVarProcessor implements EnvVarProcessorInterface
{
    private $container;

    /**
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getEnv($prefix, $name, \Closure $getEnv)
    {
        $i = strpos($name, ':');

        if ('file' === $prefix) {
            if (!is_scalar($file = $getEnv($name))) {
                throw new RuntimeException(sprintf('Invalid file name: env var "%s" is non-scalar.', $name));
            }
            if (!file_exists($file)) {
                throw new EnvNotFoundException(sprintf('Env "file:%s" not found: "%s" does not exist.', $name, $file));
            }
            return file_get_contents($file);
        }
        throw new RuntimeException(sprintf('Unsupported env var prefix "%s".', $prefix));
    }

    public static function getProvidedTypes()
    {
        return [
            'file' => 'string'
        ];
    }
}
