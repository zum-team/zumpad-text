<?php

namespace App\Service;

use \Symfony\Component\DependencyInjection\EnvVarProcessorInterface;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\DependencyInjection\Exception\EnvNotFoundException;
use \Symfony\Component\DependencyInjection\Exception\RuntimeException;



class DefaultEnvVarProcessor implements EnvVarProcessorInterface
{
    private $container;

    /**
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getEnv($prefix, $name, \Closure $getEnv)
    {
        $i = strpos($name, ':');

        if ('default' === $prefix) {
            if (false === $i) {
                throw new RuntimeException(sprintf('Invalid env "default:%s": a fallback parameter should be provided.', $name));
            }

            $next = substr($name, $i + 1);
            $default = substr($name, 0, $i);

            if ('' !== $default && !$this->container->hasParameter($default)) {
                throw new RuntimeException(sprintf('Invalid env fallback in "default:%s": parameter "%s" not found.', $name, $default));
            }

            try {
                $env = $getEnv($next);

                if ('' !== $env && null !== $env) {
                    return $env;
                }
            } catch (EnvNotFoundException $e) {
                // no-op
            }

            return '' === $default ? null : $this->container->getParameter($default);
        }
        throw new RuntimeException(sprintf('Unsupported env var prefix "%s".', $prefix));
    }

    public static function getProvidedTypes()
    {
        return [
            'default' => 'bool|int|float|string|array'
        ];
    }
}
