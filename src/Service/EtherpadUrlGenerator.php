<?php

namespace App\Service;
use Webmozart\Assert\Assert;

class EtherpadUrlGenerator
{
    private $epc;

    private $etherpad_base_url;


    public function __construct(
        string $etherpad_base_url,
        string $etherpad_api_key,
        string $etherpad_api_url
    ) {
        Assert::StringNotEmpty($etherpad_base_url);
        Assert::StringNotEmpty($etherpad_api_key);
        Assert::StringNotEmpty($etherpad_api_url);

        $this->etherpad_base_url = $etherpad_base_url;
        $this->epc = new \EtherpadLite\Client($etherpad_api_key, $etherpad_api_url);
    }

    /**
     * @param App\Entity\Pad $pad
     * @param PhpauthUsers $user
     * @param $time int seconds
     */
    public function generate_url(\App\Entity\Pad $pad, \App\Entity\PhpauthUsers $user=null, $time=null) {
        $epc = $this->epc;
        $url = $this->etherpad_base_url . "/p/".$pad->getPadID()."?showControls=true&showChat=true&showLineNumbers=true&useMonospaceFont=false";
        $time = !is_numeric($time) ? time() + (60 * 60 * 24) : time() + $time;
        if(!is_null($user) && $pad->hasAccess($user)) {
            $group = $pad->getGroup();
            if($group instanceof Group){
                $sid = $epc->createSession($group->getGroupID(), $user->getAuthorID(), $time)->sessionID;
                //$url .= "&sessionID=$sid";
                $url = $this->etherpad_base_url . "auth_session?sessionID=$sid&padName=".$pad->getPadID();//&groupID={$group->getGroupID()}
            }
        }
        return $url;
    }
}
