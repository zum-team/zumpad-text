<?php

namespace App\Service;

use \Symfony\Component\DependencyInjection\EnvVarProcessorInterface;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Symfony\Component\DependencyInjection\Exception\EnvNotFoundException;
use \Symfony\Component\DependencyInjection\Exception\ParameterCircularReferenceException;
use \Symfony\Component\DependencyInjection\Exception\RuntimeException;



class TrimEnvVarProcessor implements EnvVarProcessorInterface
{
    private $container;
    /**
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getEnv($prefix, $name, \Closure $getEnv)
    {

        $i = strpos($name, ':');
        if (false !== $i || 'string' !== $prefix) {
            if (null === $env = $getEnv($name)) {
                return null;
            }
        } elseif (isset($_ENV[$name])) {
            $env = $_ENV[$name];
        } elseif (isset($_SERVER[$name]) && 0 !== strpos($name, 'HTTP_')) {
            $env = $_SERVER[$name];
        } elseif (false === ($env = getenv($name)) || null === $env) { // null is a possible value because of thread safety issues
            if (!$this->container->hasParameter("env($name)")) {
                throw new EnvNotFoundException($name);
            }

            if (null === $env = $this->container->getParameter("env($name)")) {
                return null;
            }
        }

        if (!is_scalar($env)) {
            throw new RuntimeException(sprintf('Non-scalar env var "%s" cannot be cast to "%s".', $name, $prefix));
        }


        if ('trim' === $prefix) {
            return trim($env);
        }
        throw new RuntimeException(sprintf('Unsupported env var prefix "%s".', $prefix));
    }

    public static function getProvidedTypes()
    {
        return [
            'trim' => 'string'
        ];
    }
}
