<?php
namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="user_wrapper_news")
 * @ORM\Entity
 */
class News
{

    /**
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PhpauthUsers")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true)
     */
    private $creator;

    /**
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $created;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PhpauthUsers")
     * @ORM\JoinColumn(name="editor_id", referencedColumnName="id", nullable=true)
     */
    private $editor;

    /**
     *
     * @ORM\Column(name="edit_date", type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $edited;

    /**
     *
     * @ORM\Column(name="subject", type="string", length=200, nullable=false)
     */
    private $subject;

    /**
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct(){
        $this->created = new \DateTime();
        $this->edited = new \DateTime();
    }
    
    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    public function getCreatorName(){
        return is_null($this->getCreator()) ? "Anonymous" : $this->getCreator()->getUsername();
    }
    
    /**
     *
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created->format("Y-m-d H:i:s");
    }

    /**
     *
     * @return mixed
     */
    public function getEditor()
    {
        return $this->editor;
    }
    public function getEditorName(){
        return is_null($this->getEditor()) ? "Anonymous" : $this->getEditor()->getUsername();
    }
    
    /**
     *
     * @return mixed
     */
    public function getEdited()
    {
        return $this->edited->format("Y-m-d H:i:s");
    }

    /**
     *
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     *
     * @param mixed $editor
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;
    }

    /**
     *
     * @param mixed $edited
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;
    }

    /**
     *
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     *
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}