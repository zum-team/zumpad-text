<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="user_wrapper_roles")
 * @ORM\Entity
 */
class Role{
    use EntityTrait;
	
	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=150, nullable=false)
	 */
	protected $roleName;
	
	/**
	 * @ORM\ManyToMany(targetEntity="PhpauthUsers", mappedBy="roles")
	 */
	protected $users;
	public function __construct(){
		$this->users = new ArrayCollection();
	}
	
	/**
	 * @return string
	 */
	public function getRoleName(){
		return $this->roleName;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getUsers(){
		return $this->users;
	}
	
	/**
	 * @param string $roleName
	 */
	public function setRoleName($roleName){
		$this->roleName = $roleName;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $users
	 */
	public function setUsers($users){
		$this->users = $users;
	}
	
	public function getUserNames() {
	    return implode(', ', array_map(function(PhpauthUsers $user){return $user->getEmail();}, $this->users->toArray()));
	}
}