<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PhpauthRequests
 *
 * @ORM\Table(name="phpauth_requests", indexes={@ORM\Index(name="type", columns={"type"}), @ORM\Index(name="token", columns={"token"}), @ORM\Index(name="uid", columns={"uid"})})
 * @ORM\Entity
 */
class PhpauthRequests{
	/**
	 *
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $id;
	
	/**
	 * @ORM\Column(name="uid", type="integer", nullable=false)
	 * @ORM\ManyToOne(targetEntity="PhpauthUsers", inversedBy="authRequests")
	 * @ORM\JoinColumn(name="uid")
	 */
	private $uid;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="token", type="string", length=20, nullable=false, options={"fixed"=true})
	 */
	private $token;
	
	/**
	 * @param string $token
	 */
	public function setToken($token){
		$this->token = $token;
	}
	public function getToken(){
		return $this->token;
	}
	
	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="expire", type="datetime", nullable=false)
	 */
	private $expire;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=0, nullable=false)
	 */
	private $type;
	
	/**
	 * @return number
	 */
	public function getId(){
		return $this->id;
	}
	
	/**
	 */
	public function getUid(){
		return $this->uid;
	}
	
	/**
	 * @return DateTime
	 */
	public function getExpire(){
		return $this->expire;
	}
	
	/**
	 * @return string
	 */
	public function getType(){
		return $this->type;
	}
	
	/**
	 * @Assert\NotBlank()
	 */
	private $password;
	/**
	 * @return mixed
	 */
	public function getPassword(){
		return $this->password;
	}
	
	/**
	 * @param mixed $password
	 */
	public function setPassword($password){
		$this->password = $password;
	}
}
