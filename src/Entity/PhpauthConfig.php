<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhpauthConfig
 *
 * @ORM\Table(name="phpauth_config", uniqueConstraints={@ORM\UniqueConstraint(name="setting", columns={"setting"})})
 * @ORM\Entity
 */
class PhpauthConfig{
    /**
	 * @var string
	 * @ORM\Column(name="setting", type="string", length=100, nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $setting;
	
	/**
	 * @var string|null
	 * @ORM\Column(name="value", type="string", length=100, nullable=true)
	 */
	private $value;
	
	/**
     * @return string
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $setting
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    public function toArray() {
	    return [$this->setting=>$this->value];
	}
}
