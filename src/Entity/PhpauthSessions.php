<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhpauthSessions
 *
 * @ORM\Table(name="phpauth_sessions")
 * @ORM\Entity
 */
class PhpauthSessions{
	/**
	 *
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $id;
	
	/**
	 *
	 * @var int
	 *
	 * @ORM\Column(name="uid", type="integer", nullable=false)
	 */
	private $uid;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="hash", type="string", length=40, nullable=false, options={"fixed"=true})
	 */
	private $hash;
	
	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="expiredate", type="datetime", nullable=false)
	 */
	private $expiredate;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="ip", type="string", length=39, nullable=false)
	 */
	private $ip;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="agent", type="string", length=200, nullable=false)
	 */
	private $agent;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="cookie_crc", type="string", length=40, nullable=false, options={"fixed"=true})
	 */
	private $cookieCrc;
}
