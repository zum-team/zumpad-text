<?php

namespace App\Entity;

class Register{
	protected $email;
	protected $emailConfirm;
	protected $password;
	protected $passwordConfirm;
	/**
	 *
	 * @return mixed
	 */
	public function getEmailConfirm(){
		return $this->emailConfirm;
	}
	
	/**
	 *
	 * @return mixed
	 */
	public function getPasswordConfirm(){
		return $this->passwordConfirm;
	}
	
	/**
	 *
	 * @param mixed $emailConfirm
	 */
	public function setEmailConfirm($emailConfirm){
		$this->emailConfirm = $emailConfirm;
	}
	
	/**
	 *
	 * @param mixed $passwordConfirm
	 */
	public function setPasswordConfirm($passwordConfirm){
		$this->passwordConfirm = $passwordConfirm;
	}
	
	/**
	 *
	 * @return mixed
	 */
	public function getEmail(){
		return $this->email;
	}
	
	/**
	 *
	 * @return mixed
	 */
	public function getPassword(){
		return $this->password;
	}
	
	/**
	 *
	 * @param mixed $email
	 */
	public function setEmail($email){
		$this->email = $email;
	}
	
	/**
	 *
	 * @param mixed $password
	 */
	public function setPassword($password){
		$this->password = $password;
	}
}