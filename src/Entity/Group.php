<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;

/**
 *
 * @ORM\Table(name="user_wrapper_groups")
 * @ORM\Entity
 */
class Group{
	use EntityTrait;
	/**
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;
	
	/**
	 * @ORM\Column(name="group_id", type="string", length=18, nullable=false)
	 * Etherpad-lite groupID
	 */
	private $groupID;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PhpauthUsers")
	 * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true)
	 */
	private $owner;
	
	/**
	 * @ORM\ManyToMany(targetEntity="PhpauthUsers")
	 * @ORM\JoinTable(name="relation_authors_groups",
	 * 				joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
	 * 				inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)})
	 */
	private $authors;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Pad", mappedBy="groups")
	 */
	private $pads;
	
	/**
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	private $created;
	
	/**
	 *
	 */
	public function __construct(){
		$this->pads = new ArrayCollection();
		$this->created = new \DateTime();
	}
	/**
	 * @return mixed
	 */
	public function getName(){
		return $this->name;
	}
	public function getCreationDate(){
		return $this->created;
	}
	/**
	 * @return mixed
	 */
	public function getGroupID(){
		return $this->groupID;
	}
	
	/**
	 * @return mixed
	 */
	public function getOwner(){
		return $this->owner;
	}
	
	/**
	 * @return PersistentCollection
	 */
	public function getAuthors(){
		return $this->authors;
	}
	
	public function getAuthorEmails() {
		$mails = [];
		if(!is_null($this->getAuthors())) foreach ($this->getAuthors() AS $author) {
 			if(!$author instanceof PhpauthUsers) continue;
 			if(!in_array($author->getEmail(), $mails)) $mails[] = $author->getEmail();
 		}
		return $mails;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getPads(){
		return $this->pads;
	}

	public function hasDeleteRights(PhpauthUsers $user = null){
		if(is_null($user)) return false;
		return $this->getOwner() == $user || $user->hasRole('Admin');
	}
	
	public function hasModyfiRights(PhpauthUsers $user = null){
		if(is_null($user)) return false;
		return $this->getOwner() == $user || $user->hasRole('Admin');
	}

	public function addAuthor(PhpauthUsers $user) {
	    if(!in_array($user, $this->authors)) $this->authors[] = $user;
	}
	
	public function removeAuthor(PhpauthUsers $user) {
	    if( ($index=array_search($user, $this->authors))!==false) {
	        unset($this->authors[$index]);
	    }
	}
	/**
	 * @param mixed $name
	 */
	public function setName($name){
		$this->name = $name;
	}
	
	/**
	 * @param mixed $groupID
	 */
	public function setGroupID($groupID){
		$this->groupID = $groupID;
	}
	
	/**
	 * @param mixed $owner
	 */
	public function setOwner(PhpauthUsers $owner){
		$this->owner = $owner;
	}
	
	/**
	 * @param mixed $authorIDs
	 */
	public function setAuthorIDs($authorIDs){
		$this->authorIDs = $authorIDs;
	}
	
	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $pads
	 */
	public function setPads($pads){
		$this->pads = $pads;
	}
	
	/**
	 * @return number
	 */
	public function getId(){
		return $this->id;
	}
	public function isInGroup(PhpauthUsers $user){
		if($user->getId() === $this->owner->getId()) return true;
		foreach($this->authors as $author){
			if(!$author instanceof PhpauthUsers){
				continue;
			}
			if($author->getId() === $user->getId()) return true;
		}
		return false;
	}
}