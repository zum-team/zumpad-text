<?php
namespace App\Entity;

use Doctrine\ORM\PersistentCollection;

trait EntityTrait {
    
    static private $_toArray = null;
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id = null;
    /**
     * @return number
     */
    public function getId() {
        return intval($this->id);
    }
    public function setId(int $id) {
        if(empty($this->id)) $this->id = $id;
    }
    
    public function __toString(){
        return "".$this->getId();
    }
    
    public function toArray() {
        try {
            if(is_null(static::$_toArray)) {
                static::$_toArray = [];
                foreach (get_class_methods($this) AS $funct) {
                    if(preg_match('/^get[A-Z]{1}.*$/', $funct)) {
                        $method = new \ReflectionMethod($this, $funct);
                        if(!$method->isPublic() || !empty($method->getParameters())) continue;
                        $data = $this->{$funct}();
                        if($data instanceof PersistentCollection) {
                            $data = $data->getSnapshot();
                        }
                        if($data instanceof PhpauthUsers) {
                            $data = $data->getEmail();
                        }
                        if(is_array($data)) {
                            $data = implode(', ', $data);
                        }
                        static::$_toArray[$funct] = $data;
                    }
                }
            }
            return static::$_toArray;
        } catch (\Exception $e) {
            return [$e->__toString()];
        }
    }
    
}