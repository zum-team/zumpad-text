<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhpauthEmailsBanned
 *
 * @ORM\Table(name="phpauth_emails_banned")
 * @ORM\Entity
 */
class PhpauthEmailsBanned{
	/**
	 *
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $id;
	
	/**
	 *
	 * @var string|null
	 *
	 * @ORM\Column(name="domain", type="string", length=100, nullable=true)
	 */
	private $domain;
}
