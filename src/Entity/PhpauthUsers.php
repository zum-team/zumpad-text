<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PhpauthUsers
 * @ORM\Table(name="phpauth_users", indexes={@ORM\Index(name="email", columns={"email"})})
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email already taken")
 */
class PhpauthUsers implements UserInterface{
	use EntityTrait;
	/**
	 * @var string|null
	 * @ORM\Column(name="email", type="string", length=100, nullable=true)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;
	
	/**
	 * @var string|null
	 * @ORM\Column(name="username", type="string", length=100, nullable=true)
	 */
	private $username;
	
	/**
	 * @var string
	 * @ORM\Column(name="password", type="string", length=255, nullable=true)
	 */
	private $password;
	
	/**
	 * @var string
	 * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
	 * @ORM\JoinTable(name="relation_users_roles")
	 */
	private $roles;
	
	/**
	 * @ORM\Column(name="author_id", type="string", length=18, nullable=true)
	 * AuthorID from etherpad-lite
	 */
	private $authorID;
	
	/**
	 * @var bool
	 * @ORM\Column(name="isactive", type="boolean", nullable=false)
	 */
	private $isactive = '0';
	
	/**
	 * @ORM\ManyToMany(targetEntity="Group", inversedBy="authors")
	 */
	private $groups;
	
	/**
	 * @ORM\Column(name="dt", type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
	 */
	private $created;
	
	private $plainPassword;
	
	public function __construct(){
		$this->groups = new ArrayCollection();
		$this->roles = new ArrayCollection();
		$this->created = new \DateTime();
	}
	
	/**
	 * @return mixed
	 */
	public function getAuthorID(){
		return $this->authorID;
	}
	
	/**
	 * @param mixed $authorID
	 */
	public function setAuthorID($authorID){
		$this->authorID = $authorID;
	}
	
	/**
	 * @return string
	 */
	public function getEmail(){
		return $this->email;
	}
	
	/**
	 * @return string
	 */
	public function getPlainPassword(){
		return $this->plainPassword;
	}
	
	/**
	 * @return string
	 */
	public function getPassword(){
		return $this->password;
	}
	
	/**
	 * @return multitype:string
	 */
	public function getRoles(){
		return $this->roles;
	}
	
	/**
	 * @return boolean
	 */
	public function isIsactive(){
		return $this->isactive;
	}
	
	/**
	 * @param
	 * Ambigous <string, NULL> $email
	 */
	public function setEmail($email){
		$this->email = $email;
	}
	
	/**
	 * @param string $plainPassword
	 */
	public function setPlainPassword($plainPassword){
		$this->plainPassword = $plainPassword;
	}
	
	/**
	 * @param string $password
	 */
	public function setPassword($password){
		$this->password = $password;
	}
	
	/**
	 * @param multitype:string $roles
	 */
	public function setRoles(Role $roles){
	    if(!$this->roles->contains($roles)) $this->roles->add($roles);
	}
	
	/**
	 * @param boolean $isactive
	 */
	public function setIsactive($isactive){
		$this->isactive = $isactive;
	}
	public function eraseCredentials(){}
	public function getSalt(){}
	public function getUsername(){
		return $this->username;
	}
	
	/**
	 * @ORM\ManyToMany(targetEntity="Pad" , inversedBy="authors")
	 */
	public $subscribed_pads;
	
	/**
	 * @ORM\OneToMany(targetEntity="PhpauthRequests", mappedBy="uid")
	 */
	public $authRequests;
	public function hasRole($role){
		foreach($this->getRoles() as $r){
			if($r->getRolename() == $role) return true;
		}
		return false;
	}
}
