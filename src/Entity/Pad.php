<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\AuthController;
use PHPAuth\Config;
use PHPAuth\Auth;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\Common\Collections\ArrayCollection;
use EtherpadLite\Client;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Common\Collections\Collection;

/**
 *
 * @ORM\Table(name="user_wrapper_pads")
 * @ORM\Entity
 */
class Pad{
    use EntityTrait;
	/**
	 * @ORM\ManyToMany(targetEntity="PhpauthUsers" , inversedBy="subscribed_pads")
	 * @ORM\JoinTable(name="relation_authors_pads")
	 */
	private $authors;
	/**
	 * @ORM\ManyToMany(targetEntity="Group", inversedBy="pads")
	 * @ORM\JoinTable(name="relation_pads_groups")
	 */
	private $groups;
	/**
	 * @ORM\Column(name="is_password_protected", type="boolean", nullable=false)
	 */
	private $isPasswordProtected;
	/**
	 * @ORM\Column(name="name", type="string", length=200, nullable=false)
	 */
	private $name;
	/**
	 * @ORM\ManyToOne(targetEntity="PhpauthUsers")
	 * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true)
	 */
	private $owner;
	/**
	 * @ORM\Column(name="pad_id", type="string", length=219, nullable=false)
	 * etherpad-lite etherpadID
	 */
	private $padID;
	/**
	 * @ORM\Column(name="is_private", type="boolean", nullable=false)
	 * @var boolean
	 */
	private $private;
	
	/**
	 * @var bool
	 */
	protected $newGroup;
	/**
	 * @Assert\Length(max=4096)
	 * @var string
	 */
	protected $password;
	
	public function __construct(){
		$this->authors = new ArrayCollection();
		$this->groups = new ArrayCollection();
		$this->isPasswordProtected = false;
	}
	
	/**
	 * @param PhpauthUsers $author
	 * @return \App\Entity\Pad
	 */
	public function addAuthor(PhpauthUsers $author){
		if(!$this->authors->contains($author)) $this->authors->add($author);
		return $this;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getAuthors(){
		return $this->authors;
	}

	/**
	 * @return mixed
	 */
	public function getGroup(){
		if(!$this->groups instanceof Collection) return null;
		if(!$this->groups->isEmpty()) return $this->groups->first();
		else return null;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getGroups(){
		return $this->groups;
	}
	
	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}
	
	/**
	 * @return boolean
	 */
	public function getIsPasswordProtected(){
		return $this->isPasswordProtected===true;
	}
	
	public function getName(){
		return $this->name;
	}
	public function getOwner(){
		return $this->owner;
	}
	public function getPadID(){
		return $this->padID;
	}
	public function getPassword(){
		return $this->password;
	}
	/*
	 * TODO:
	 */
	public function getTitle() {
		$matches = null;
		$pattern = "/^([g]{1}[\.]{1}[a-zA-Z0-9]{16}[\$]{1})/";
		return preg_replace($pattern, '', $this->getPadID());
	}
	public function hasAccess(PhpauthUsers $user = null){
	    $has_access = false;
		if(!$this->isPrivate()){
			$has_access = true;
		}elseif($user == $this->getOwner()){
			$has_access = true;
		}elseif($user->hasRole("Admin")){
			$has_access = true;
		}elseif($this->hasAuthor($user->getAuthorID())){
			$has_access = true;
		}elseif($this->getIsPasswordProtected()) {
			$has_access = true;
		}
		return $has_access;
	}
	
	public function hasAuthor($authorID){
	    foreach($this->getAuthors() as $a){
		    if($a->getAuthorID() == $authorID) {
		        return true;
		    }
		}
		foreach ($this->getGroup()->getAuthors() AS $author) {
		    if($author instanceof PhpauthUsers) {
		        if($author->getAuthorID()===$authorID) return true;
		    }
		}
		foreach ($this->getGroups() AS $group){
		    if($group instanceof Group) {
		        foreach ($group->getAuthors() AS $author) {
		            if($author instanceof PhpauthUsers) {
		                if($author->getAuthorID()===$authorID) return true;
		            }
		        }
		    }
		}
		return false;
	}
	public function hasDeleteRights(PhpauthUsers $user = null){
		if(is_null($user)) return false;
		return $this->getOwner() == $user || $user->hasRole('Admin');
	}
	public function hasModyfiRights(PhpauthUsers $user = null){
		if(is_null($user)) return false;
		return $this->getOwner() == $user || $user->hasRole('Admin');
	}
	
	public function isNewGroup(){
		return $this->newGroup;
	}
	
	public function isPrivate(){
		return $this->private;
	}
	
	public function setGroup(Group $group){
		if(!is_null($group) &&  !$this->groups->contains($group)) $this->groups->add($group);
		return $this;
	}
	public function setGroups($groups){
		if(!is_null($groups)) $this->groups = $groups;
		else $this->groups = new ArrayCollection();
		return $this;
	}
	
	public function setId(int $id){
		$this->id = $id;
		return $this;
	}

	public function setIsPasswordProtected($isPasswordProtected=null){
		$this->isPasswordProtected = $isPasswordProtected === true ? true : false;
		return $this;
	}
	public function setName($name){
		$this->name = $name;
		return $this;
	}
	public function setNewGroup($newGroup){
		$this->newGroup = $newGroup;
		return $this;
	}
	public function setOwner(PhpauthUsers $owner){
		$this->owner = $owner;
		return $this;
	}
	public function setPadID($padID){
		$this->padID = $padID;
		return $this;
	}
	public function setPassword($password){
		$this->password = $password;
		return $this;
	}
	public function setPrivate($private){
		$this->private = $private;
		return $this;
	}
	
	public static function persist(Client $epc, Pad $pad) {
		$result = false;
		try {
			//if publicStatus than isPrivate has to be false;  if !publicStatus than isPrivate has to be true
			if($epc->getPublicStatus(!$pad->getPadID())->publicStatus !== $pad->isPrivate()) {
				$epc->setPublicStatus($pad->getPadID(), !$pad->isPrivate());
			}
			
			if($pad->getIsPasswordProtected() && $epc->isPasswordProtected($pad->getPadID())->isPasswordProtected !== $pad->getIsPasswordProtected()) {
				$epc->setPassword($pad->getPadID(), $pad->getPassword());
			} elseif(!$pad->getIsPasswordProtected() && $epc->isPasswordProtected($pad->getPadID())->isPasswordProtected !== $pad->getIsPasswordProtected()) {
				$epc->setPassword($pad->getPadID(), null);
			}
			$result = true;
		} catch (\Exception $e) {
			return $e->getMessage();
		}
		return $result;
	}
}
