<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhpauthAttempts
 *
 * @ORM\Table(name="phpauth_attempts", indexes={@ORM\Index(name="ip", columns={"ip"})})
 * @ORM\Entity
 */
class PhpauthAttempts{
	/**
	 *
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	private $id = null;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="ip", type="string", length=39, nullable=false, options={"fixed"=true})
	 */
	private $ip;
	
	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="expiredate", type="datetime", nullable=false)
	 */
	private $expiredate;
}
