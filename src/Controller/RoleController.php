<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\PhpauthUsers;
use App\Entity\Role;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RoleController extends UserWrapperController {
    
    private function _get_role_form(Role $role) {
        $fb = $this->createFormBuilder($role);
        $fb->add('id', HiddenType::class,[
            'label'=>"Id",
        ]);
        $fb->add('roleName', TextType::class, [
            'label'=>"Name"
        ]);
        $fb->add('users', EntityType::class, [
            'class' => PhpauthUsers::class,
            'choice_label' => 'getEmail',
//             'choice_value' => function (PhpauthUsers $entity = null) {
//             return $entity ? $entity->getId() : 0;
//             },
            'multiple'=>true,
            ]);
        $fb->add("save", SubmitType::class, [
        "label" => 'Speichern'
        ]);
        return $fb->getForm();
    }
    
    /**
     * @Route("/roles/{roleId}" , name="roles")
     */
    public function list_rolls(Request $request, $roleId=0) {
        $user = $this->user;
        if(!$user instanceof PhpauthUsers || !$user->hasRole('Admin')) return self::forbidden($this);
        if(!$request->isSecure()) return self::upgrade_required($this);
        $em = $this->getDoctrine()->getManager();
        $role_repo = $em->getRepository(Role::class);
        $role = empty($roleId) ? null : $role_repo->find($roleId);
        if(is_null($role)) {
            $role = new Role();
            $data = ["roles"=>$role_repo->findAll()];
        }else{
            //$request->
        }
        $form = $this->_get_role_form($role);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            foreach ($role->getUsers() AS $user) {
                var_dump($user->getEmail(), $user->getRoles());
            }
            
            $em->persist($role);
            $em->flush();
            $role_check = empty($roleId) ? null : $role_repo->find($roleId);
            if($role_check instanceof Role) var_dump($role_check->getUserNames());
        }
        if(!empty($roleId)) {
            return $this->render("edit_role.html.twig",  array_merge($this->to_array(), ['form' => $form->createView()]));
        }
        return $this->render("list_roles.html.twig",  array_merge($this->to_array(), $data));
    }
    
}