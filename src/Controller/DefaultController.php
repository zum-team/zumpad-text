<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\PhpauthConfig;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\PhpauthUsers;
use App\Entity\Role;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App;

class DefaultController extends UserWrapperController{
	
	/**
	 *
	 * @Route("/", name="user_wrapper")
	 */
    public function index(PadController $padController, AuthController $authController){
	    $data = [
	        'quick_create_pad_form' => $padController->get_quick_create_form()->createView(),
	        'register_form' => $authController->get_register_form()->createView(),
	        'login_form'=> $authController->get_login_form()->createView(),
        ];
	    
	    if(!is_null($authController->user)) {
	        if(count($authController->get_epc()->listPadsOfAuthor($authController->user->getAuthorID())->padIDs)>0) {
	            return $this->redirectToRoute("list_pads");
	        }
	        return $this->redirectToRoute("create_pad");
	    }
	        
	    return $this->render("home.html.twig",  array_merge($this->to_array(), $data));
	}
	
	
	/**
	 * @Route("/config" , name="config")
	 */
	public function config(Request $request) {
	    $user = $this->user;
	    if(!$user instanceof PhpauthUsers || !$user->hasRole('Admin')) return self::forbidden($this);
	    if(!$request->isSecure()) return self::upgrade_required($this);

	    $fb = $this->createFormBuilder();
	    $em = $this->getDoctrine()->getManager();
	    $config_repo = $em->getRepository(PhpauthConfig::class);

	    foreach ($config_repo->findBy([]) as $config) {
	        if(!$config instanceof PhpauthConfig) continue;
	        $fb->add($config->getSetting(), TextType::class,[
	            'label'=>$config->getSetting(),
	            'data'=>$config->getValue(),
	            'required'=> false,
	        ]);
	    }
	    $fb->add("save", SubmitType::class, [
	        "label" => 'Speichern'
	    ]);
	    $form = $fb->getForm();
	    $form->handleRequest($request);
	    if($form->isSubmitted() && $form->isValid()) {
	        foreach ($form->getData() AS $key=>$value) {
	            $entity = $config_repo->findOneBy(['setting'=>$key]);
	            if(!$entity instanceof PhpauthConfig) continue;
	            if(!is_null($value) && $entity->getValue() !== $value) {
 	                $entity->setValue($value);
 	                $em->persist($entity);
 	                $em->flush();
 	            }
	        }
	    }
	    return $this->render("config.html.twig",  array_merge($this->to_array(), ['form' => $form->createView()]));
	}
}
    