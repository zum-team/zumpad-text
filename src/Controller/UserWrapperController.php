<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use PHPAuth\Auth;
use PHPAuth\Config;
use EtherpadLite\Client;
use Doctrine\DBAL\Driver\Connection;
use App\Entity\Register;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Form\Tests\Extension\Core\Type\PasswordTypeTest;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use App\Entity\PhpauthUsers;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class UserWrapperController extends AbstractController{
	const FORCE = 0x1;
	private $_data;
	private $_url_generator_interface;
	private $_request;
	private $_pho_data_object;
	private $_php_auth_config;
	private $_php_auth_object;
	private $_ether_pad_client;
	private $_ether_pad_url_generator;
	private $_notices;
	private $_warings;
	private $_errors;
	
	/**
	 *
	 * @return \Symfony\Component\Routing\Generator\UrlGeneratorInterface
	 */
	public function get_route(){
		return $this->_url_generator_interface;
	}
	
	/**
	 *
	 * @return \Doctrine\DBAL\Driver\Connection
	 */
	public function get_pdo(){
		return $this->_pho_data_object;
	}
	
	/**
	 *
	 * @return \PHPAuth\Auth
	 */
	public function get_auth(){
	    return $this->_php_auth_object;
	}
	
	/**
	 *
	 * @return \EtherpadLite\Client
	 */
	public function get_epc(){
		return $this->_ether_pad_client;
	}

	protected function get_etherpad_url_generator():\App\Service\EtherpadUrlGenerator {
		return $this->_ether_pad_url_generator;
	}

	public function __construct(
		string $etherpad_api_key,
		string $etherpad_api_url,
		Connection $conn,
		UrlGeneratorInterface $router,
        \App\Service\EtherpadUrlGenerator $etherpad_url_generator
	){
		$this->_url_generator_interface = $router;
		$this->_pho_data_object = $conn->getWrappedConnection();
		$this->_php_auth_config = new Config($this->_pho_data_object);
		$this->_php_auth_object = new Auth($this->_pho_data_object, $this->_php_auth_config, "de_DE");
		$this->_ether_pad_client = new Client($etherpad_api_key, $etherpad_api_url);
        $this->_ether_pad_url_generator = $etherpad_url_generator;
		$this->_notices = [];
		$this->_warings = [];
		$this->_errors = [];
		
		$this->_data = [];
		$this->_data['uid'] = $this->_php_auth_object->getSessionUID($this->_php_auth_object->getCurrentSessionHash());
		$this->_data['logged_in'] = $this->_php_auth_object->isLogged();
		$this->_data['debug'] = filter_input(INPUT_GET, 'debug', FILTER_SANITIZE_NUMBER_INT)==1;
		$this->_data['debug_ajax'] = filter_input(INPUT_GET, 'debug_ajax', FILTER_SANITIZE_NUMBER_INT)==1;
		$this->_data['uniqid'] = uniqid();
	}
	public function __get($name){
		if(array_key_exists($name, $this->_data)) return $this->_data[$name];
		switch($name){
			case 'epc':
			case 'ether_pad_client':
				return $this->_ether_pad_client;
			case 'auth_cfg':
			case 'php_auth_config':
				return $this->_php_auth_config;
			case 'auth':
			case 'php_auth_object':
				return $this->_php_auth_object;
			case 'pdo':
			case 'pho_data_object':
				return $this->_pho_data_object;
			case 'router':
			case 'url_generator_interface':
				return $this->_url_generator_interface;
			case 'user':
				return $this->_php_auth_object->isLogged() ? $this->getDoctrine()->getManager()->getRepository(PhpauthUsers::class)->find($this->_data['uid']) : null;
		}
		return null;
	}
	public function set($key, $value){
		if(array_key_exists($key, $this->_data)) $this->add_notice("Key exists $key");
		if(!empty($this->_data[$key])) $this->add_notice("Overwrite $key");
		$this->_data[$key] = $value;
	}
	public function add_notice($string){
		$this->_notices[] = $string . PHP_EOL;
	}
	public function add_waring($string){
		$this->_warings[] = $string . PHP_EOL;
	}
	public function add_error($string){
		$this->_errors[] = $string . PHP_EOL;
	}
	public function to_array(){
		if($this->_php_auth_object->isLogged()){
			$em = $this->getDoctrine()->getManager();
			$usr_repo = $em->getRepository(PhpauthUsers::class);
			$this->_data['user'] = $usr_repo->find($this->_data['uid']);
		}else{
			$this->_data['user'] = new PhpauthUsers();
		}
		return $this->_data;
	}
	public static function forbidden(UserWrapperController $controler) {
		$response = new Response();
		$response->setStatusCode(Response::HTTP_FORBIDDEN);
		$controler->add_error("No access");
		return $controler->render("403_forbidden.html.twig", $controler->to_array(), $response);
	}
	public static function bad_request(UserWrapperController $controler){
		$response = new Response();
		$response->setStatusCode(Response::HTTP_BAD_REQUEST);
		$controler->add_error("No Pad found");
		return $controler->render("400_bad_request.html.twig", $controler->to_array(), $response);
	}
	public static function upgrade_required(UserWrapperController $controler) {
	    $response = new Response();
	    $response->setStatusCode(Response::HTTP_UPGRADE_REQUIRED);
	    $controler->add_error("Upgrade required");
	    return $controler->render("426_upgrade_required.html.twig", $controler->to_array(), $response);
	}
}
