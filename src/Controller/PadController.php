<?php
namespace App\Controller;

use \DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\Entity\Pad;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Group;
use App\Entity\PhpauthUsers;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\Criteria;
use App\Entity\Role;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Endroid\QrCode\QrCode;
use EtherpadLite\Client;
use Endroid\QrCode\Response\QrCodeResponse;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\ErrorCorrectionLevel;

class PadController extends UserWrapperController
{

    const E_POOL = 0;

    const E_AUTHOR = 1;

    const E_LAST_EDITED = 2;

    const E_IS_PASSWORD_PROTEECTED = 3;

    const E_REVISIONS = 4;

    const E_PUBLIC = 5;

    const E_READONLY = 6;

    const E_BLUBB = 7;

    public function get_new_pad_form(Pad $pad)
    {
        $fb = $this->createFormBuilder($pad);
        $fb->add("name", TextType::class, [
            "label" => 'Name des Pads'
        ]);
        if ($this->logged_in) {
            $fb->add("private", CheckboxType::class, [
                "label" => 'Privates Pad',
                "required" => false,
                'value' => $this->get_auth()
                    ->isLogged(),
            ]);
            $fb->add("isPasswordProtected", ChoiceType::class, [
                "label" => "durch Passwort geschützt",
                "choices" => [
                    "Ja" => true,
                    "Nein" => false
                ],
                "required" => true
            ]);
            $fb->add("password", RepeatedType::class, [
                "type" => PasswordType::class,
                "options" => [
                    "attr" => [
                        "class" => 'password-field'
                    ]
                ],
                "required" => false,
                "first_name" => "password",
                "first_options" => [
                    "label" => 'Passwort'
                ],
                "second_name" => 'passwordConfirm',
                "second_options" => [
                    "label" => 'Passwort zur Kontrolle'
                ]
            ]);
        }
        $fb->add("save", SubmitType::class, [
            "label" => 'Erstellen'
        ]);
        return $fb->getForm();
    }

    public function get_pad_edit_form(Pad $pad)
    {
        $fb = $this->createFormBuilder($pad);
        $fb->add("id", HiddenType::class)->add("name", TextType::class, [
            "label" => 'Name'
        ]);
        $fb->add("private", CheckboxType::class, [
            "label" => 'Privates Pad',
            "required" => false,
            "disabled" => true
        ]);
        $fb->add("isPasswordProtected", ChoiceType::class, [
            "label" => "durch Passwort geschützt",
            "choices" => [
                "Ja" => true,
                "Nein" => false
            ],
            "required" => true
        ]);
        $fb->add("password", RepeatedType::class, [
            "type" => PasswordType::class,
            "options" => [
                "attr" => [
                    "class" => 'password-field'
                ]
            ],
            "required" => false,
            "first_name" => "password",
            "first_options" => [
                "label" => 'Passwort'
            ],
            "second_name" => 'passwordConfirm',
            "second_options" => [
                "label" => 'Passwort wiederholen'
            ]
        ]);
        $fb->add('save', SubmitType::class, [
            'label' => 'Speichern'
        ]);
        return $fb->getForm();
    }

    public function get_quick_create_form(Pad $pad=null) {
        $form_pad = is_null($pad) ? new Pad() : $pad;
        $fb = $this->createFormBuilder($form_pad);
        $fb->add("name", TextType::class, [
            "label" => 'Name des Pads'
        ]);
        $fb->add('save', SubmitType::class, [
            'label' => 'Pad erstellen',
            'attr' => ['class' => 'btn secondary'],
        ]);
        return $fb->getForm();
    }
    
    /**
     *
     * @Route("/create_pad" , name="create_pad")
     */
    public function create_pad(Request $request)
    {
        $i = 1;
        $em = $this->getDoctrine()->getManager();
        $em->clear();
        $repo = $em->getRepository(Pad::class);

        $pad = new Pad();

        $form = $this->get_new_pad_form($pad);
        $form->handleRequest($request);
        $data = $request->get("form");
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->user;
            if (! $pad->isPrivate()) { // Public pad
                try {
                    $name = $pad->getName();
                    $this->get_epc()->createPad($name);
                    $pad->setPadID($name);
                    $pad->setPrivate(false);
                    $pad->setIsPasswordProtected(false);
                    $none_user = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(PhpauthUsers::class)
                        ->findOneBy([
                        'email' => UpdateController::USER_NONE
                    ]);
                    $pad->setOwner($none_user);
                    $em->persist($pad);
                    $em->flush();
                    // return $this->redirectToRoute("list_pads");
                    return $this->redirectToRoute('show_pad', [
                        'padID' => $pad->getPadID()
                    ]);
                } catch (\Exception $e) {
                    $form->addError(new FormError(str_replace("padID does already exist", "name does already exist", $e->getMessage())));
                }
            } elseif (! $this->get_auth()->isLogged() || ! ($user instanceof PhpauthUsers)) {
                $form->addError(new FormError("Not logged in."));
            } else { // Private pad
                $pad->setOwner($user);
                $dt = new \DateTime();
                $gid = "{$dt->format('Y-m-d H:i:s')} {$pad->getName()}";
                $real_group_id = $this->get_epc()->createGroupIfNotExistsFor($gid)->groupID;
                $group = new Group();
                $group->setGroupID($real_group_id);
                $group->setOwner($user);
                $group->setName("[Gruppe]" . $pad->getName());
                $em->persist($group);
                $em->flush();
                $pad->setGroup($group);
                try {
                    $real_pad_id = $this->get_epc()->createGroupPad($real_group_id, $pad->getName())->padID;
                    $pad->setPadID($real_pad_id);
                    if ($pad->getIsPasswordProtected()) {
                        $this->get_epc()->setPassword($real_pad_id, $pad->getPassword());
                    }
                    $em->persist($pad);
                    $em->flush();
                    // return $this->redirectToRoute("list_pads");
                    return $this->redirectToRoute('show_pad', [
                        'padID' => $pad->getPadID()
                    ]);
                } catch (\Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                }
            }
        }

        return $this->render("create_pad.html.twig", array_merge([
            'form' => $form->createView()
        ], $this->to_array()));
    }

    /**
     *
     * @Route("/list_pads", name="list_pads")
     */
    public function list_pads(Request $request)
    {
        // if user is not loggedin only public pads will be shown
        if (! $this->get_auth()->isLogged()) {
            return $this->forbidden($this);
        }
        $em = $this->getDoctrine()->getManager();
        $pad_repo = $em->getRepository(Pad::class);
        $criteria = [];
        $data = [];
        foreach ($pad_repo->findBy($criteria) as $pad) {
            if (! $pad instanceof Pad) continue;
            try {
                $this->get_epc()->getRevisionsCount($pad->getPadID());
            } catch (\Exception $e) { // Pad does not exsist
                $this->getDoctrine()
                    ->getManager()
                    ->remove($pad);
                $this->getDoctrine()
                    ->getManager()
                    ->flush();
                continue;
            }
            if ($pad->hasAccess($this->user)) {
                $pool = null;
                try {
                    $pool = $this->get_epc()->getAttributePool($pad->getPadID())->pool;
                } catch (\Exception $e) {
                    $data['code'] = self::E_POOL;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }
                // $author = null;
                // try {
                //     $author = implode(', ', $this->get_epc()->listAuthorsOfPad($pad->getPadID())->authorIDs);
                // } catch (\Exception $e) {
                //     $data['code'] = self::E_AUTHOR;
                //     $data['msg'] = $e->getMessage();
                //     $data['trace'] = $e->getTrace();
                // }
                $lastEdited = null;
                try {
                    $lastEdited = $this->get_epc()->getLastEdited($pad->getPadID())->lastEdited;
                } catch (\Exception $e) {
                    $data['code'] = self::E_LAST_EDITED;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }
                $lastEdited = DateTimeImmutable::createFromFormat("U", $lastEdited / 1000);
                $isPasswordProtected = false;
                try {
                    $isPasswordProtected = $this->get_epc()->isPasswordProtected($pad->getPadID())->isPasswordProtected;
                } catch (\Exception $e) {
                    $data['code'] = self::E_IS_PASSWORD_PROTEECTED;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }
                $revisions = 0;
                try {
                    $revisions = $this->get_epc()->getRevisionsCount($pad->getPadID())->revisions;
                } catch (\Exception $e) {
                    $data['code'] = self::E_REVISIONS;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }
                $public = true;
                try {
                    $public = $this->get_epc()->getPublicStatus($pad->getPadID())->publicStatus;
                } catch (\Exception $e) {
                    $data['code'] = self::E_PUBLIC;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }
                $readonly = null;
                try {
                    $readonly = $this->get_epc()->getReadOnlyID($pad->getPadID())->readOnlyID;
                } catch (\Exception $e) {
                    $data['code'] = self::E_READONLY;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }
                $blubb = null;
                try {
                    $blubb = $this->get_epc()->getSavedRevisionsCount($pad->getPadID());
                } catch (\Exception $e) {
                    $data['code'] = self::E_BLUBB;
                    $data['msg'] = $e->getMessage();
                    $data['trace'] = $e->getTrace();
                }

                $url = $this->generateUrl('show_pad', [
                    'padID' => urlencode($pad->getPadID())
                ]);
                $data['data'][] = [
                    'id' => $pad->getId(),
                    'padId' => $pad->getPadID(),
                    'title' => "<a href='$url' target='_blank'>{$pad->getName()}</a>",
                    'url' => $url,
                    'name' => $pad->getName(),
                    // 'authors' => $author,
                    'lastEdited' => $lastEdited,
                    'isPublic' => $public,
                    'isPasswordProtected' => $isPasswordProtected,
                    'isReadOnly' => $readonly,
                    'revisions' => $revisions,
                    'misc' => $pool,
                    'is_deletable' => $pad->hasDeleteRights($this->user),
                    'is_deleteable' => $pad->hasDeleteRights($this->user),
                    'show_options' => $pad->hasModyfiRights($this->user)
                ];
            }
        }

        if ($request->isXmlHttpRequest() || $this->debug_ajax) {
            return new JsonResponse($data);
        } else {
            return $this->render("list_pads.html.twig", array_merge($this->to_array(), $data));
        }
    }

    /**
     *
     * @Route("/edit_pad/{padID}", name="edit_pad")
     */
    public function edit_pad($padID = null, Request $request)
    {
        if (is_null($padID)) {
            $data = $request->get("form");
            if (array_key_exists('id', $data)) {
                $pad = $this->getDoctrine()
                    ->getManager()
                    ->getRepository(Pad::class)
                    ->find($data['id']);
            } else {
                $pad = new Pad();
            }
        } else {
            $pad = $this->getDoctrine()
                ->getManager()
                ->getRepository(Pad::class)
                ->findOneBy([
                'padID' => $padID
            ]);
        }
        if (! $pad instanceof Pad) {
            return self::bad_request($this);
        }
        if (! $pad->hasModyfiRights($this->user)) {
            return self::forbidden($this);
        }
        $form = $this->get_pad_edit_form($pad);
        $form->handleRequest($request);
        if ($request->isXmlHttpRequest() || $this->debug_ajax) {
            return $this->render("edit_pad.html.twig", [
                'form' => $form->createView()
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // private status change?
            if ($pad->isPrivate()) {
                // TODO: Some magic stuff...
            }
            $user = $this->user;
            if (! $pad->isPrivate()) {
                // TODO: check what to do
            } elseif (! $this->get_auth()->isLogged() || ! ($user instanceof PhpauthUsers)) {
                $this->add_error("Not logged in");
                return $this->render("403_forbidden.html");
            } else {
                // TODO: check what to do
            }
            if ($pad->getIsPasswordProtected()) {
                $this->get_epc()->setPassword($pad->getPadID(), $pad->getPassword());
            }
            if ($check = Pad::persist($this->get_epc(), $pad) !== true) {
                $form->addError(new FormError($check));
                $this->set('form', $form);
                $em->persist($pad);
                $em->flush();
                return $this->redirectToRoute('list_pads');
            }
        }
        return $this->show_pads($request);
    }

    /**
     *
     * @Route("/delete_pad/{padID}", name="delete_pad")
     */
    public function delete_pad($padID = null, Request $request)
    {
        if (is_null($padID))
            return new Response("No data.");
        $pad = $this->getDoctrine()
            ->getManager()
            ->getRepository(Pad::class)
            ->findOneBy([
            'padID' => $padID
        ]);
        if (is_null($pad) || ! $pad instanceof Pad) {
            return new JsonResponse([
                'status' => "ERROR",
                'msg' => "No Pad with that id found."
            ]);
        }
        // check if user has ability to delete pad
        $user = $this->user;
        if (is_null($user) || ! $user instanceof PhpauthUsers) {
            return new JsonResponse([
                'status' => "ERROR",
                'msg' => "No User found."
            ]);
        }
        if ($pad->hasDeleteRights($user)) {
            try {
                $this->get_epc()->deletePad($pad->getPadID());
                $this->getDoctrine()
                    ->getManager()
                    ->remove($pad);
                $this->getDoctrine()
                    ->getManager()
                    ->flush();
            } catch (\Exception $e) {
                return new JsonResponse([
                    'status' => "ERROR",
                    'msg' => $e->getMessage()
                ]);
            }
        } else {
            return new JsonResponse([
                'status' => "ERROR",
                'msg' => "User has no power to delete this pad."
            ]);
        }
        return new JsonResponse([
            'status' => "OK"
        ]);
        // if( $request->isXmlHttpRequest() ){
        // return new JsonResponse( [] );
        // }
    }

    /**
     * @Route("/p/{padID}", name="show_pad")
     */
    public function show_pad($padID = null, Request $request)
    {
        $padName = $request->request->has('form')  ? $request->request->get('form')['name'] : null;
        if(is_null($padID) && !empty($padName)) {
            return $this->redirect('p/'.$padName);
        }
        $padID = urldecode($padID);
        $em = $this->getDoctrine()->getManager();
        $pad_repo = $em->getRepository(Pad::class);
        $pad = $pad_repo->findOneBy([
            'padID' => $padID
        ]);
        if(is_null($pad)) {
            $pad = new Pad();
            $pad->setPadID($padID);
            $pad->setIsPasswordProtected(false);
            $pad->setName($padID);
            $pad->setPrivate(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($pad);
            $em->flush();
        }
        
        if (! is_null($pad) && $pad instanceof Pad) {
        	if(is_null($this->user)) {
        		$this->user = new PhpauthUsers();
        	}
        	if(is_null($this->user->getAuthorID()) && $pad->getIsPasswordProtected()) {
        		$author_id = $this->epc->createAuthor(uniqid())->authorID;
        		$this->user->setAuthorID($author_id);
        		$this->user->setEmail ( "guest_".$author_id."@zum.de" );
        		$this->user->setIsactive ( 1 );
        		$guest = $em->getRepository(Role::class)->findOneBy(["roleName"=>"Guest"]);
        		if($guest instanceof Role) $this->user->setRoles($guest);
        		$pad->addAuthor($this->user);
        		$em->persist ( $this->user );
        		$em->persist($pad);
        	}
        	$url = $this->get_etherpad_url_generator()->generate_url($pad, $this->user);
        	$em->remove($this->user);
       		//$em->flush();
        }

        return $this->render("zumpad.html.twig", array_merge([
            'pad_url' => $url
        ], $this->to_array()));
    }
    
    /**
     *
     * @Route("/qr/{padID}", name="qr_pad")
     */
    public function qr_pad($padID = null)
    {
        $em = $this->getDoctrine()->getManager();
        $pad_repo = $em->getRepository(Pad::class);
        $pad = $pad_repo->findOneBy([
            'padID' => $padID
        ]);
        if (! is_null($pad) && $pad instanceof Pad) {
            $qrCode = new QrCode($this->get_etherpad_url_generator()->generate_url($pad, $this->user));
        } else {
            $qrCode = new QrCode('Error');
        }
        $qrCode->setSize(300);

        // Set advanced options
        $qrCode->setWriterByName('png');
        $qrCode->setMargin(10);
        $qrCode->setEncoding('UTF-8');
        $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
        $qrCode->setForegroundColor([
            'r' => 0,
            'g' => 0,
            'b' => 0,
            'a' => 0
        ]);
        $qrCode->setBackgroundColor([
            'r' => 255,
            'g' => 255,
            'b' => 255,
            'a' => 0
        ]);
        $qrCode->setLabel('Scan the code'); // , 16, __DIR__.'/../../assets/fonts/noto_sans.otf', LabelAlignment::CENTER
                                            // $qrCode->setLogoPath(__DIR__.'/../assets/images/symfony.png');
        $qrCode->setLogoSize(150, 200);
        $qrCode->setRoundBlockSize(true);
        $qrCode->setValidateResult(false);
        $qrCode->setWriterOptions([
            'exclude_xml_declaration' => true
        ]);

        // Directly output the QR code
        header('Content-Type: ' . $qrCode->getContentType());
        echo $qrCode->writeString();

        // Save it to a file
        $qrCode->writeFile(__DIR__ . '/qrcode.png');

        // Create a response object
        $response = new QrCodeResponse($qrCode);
        return $response;
    }
}
