<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\DBAL\Driver\Connection;
use App\Entity\Role;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Group;
use EtherpadLite\Client;
use App\Entity\PhpauthUsers;

class UpdateController extends AbstractController {


	/**
	 *  @type \EtherpadLite\Client
     */
	private $epc;

	public function __construct(
		string $etherpad_api_key,
		string $etherpad_api_url
	){
		$this->epc = new Client($etherpad_api_key, $etherpad_api_url);
	}

	/**
	 *
	 * @Route("/git_pull" , name="git_pull")
	 */
	public function git_pull(Request $request) {
		try {
			if (array_key_exists ( 'HTTP_X_GITLAB_TOKEN', $_SERVER ) && $_SERVER ["HTTP_X_GITLAB_TOKEN"] == getenv ( 'GITLAB_SECRET' )) {
				$exit = null;
				$out = [ ];
				$path = dirname ( dirname ( __DIR__ ) );
				try {
					exec ( "cd $path && git pull 2>&1", $out, $exit );
					while ( ! is_numeric ( $exit ) ) {
						sleep ( 1 );
					}
				} catch ( \Exception $e ) {
					return new JsonResponse ( [ 
							'Status' => $exit,
							'MSG' => $e->getMessage ()
					] );
				}
				return new JsonResponse ( [ 
						'Status' => $exit,
						'MSG' => $out
				] );
			} else {
				return new JsonResponse ( [ 
						'Status' => "ERROR",
						'MSG' => "Forbidden"
				], 403 );
			}
		} catch ( \Exception $e ) {
			return new JsonResponse ( [ 
					'Status' => $exit,
					'MSG' => $e->getMessage ()
			] );
		}
	}
	const DEFAULT_ROLES = [ 
			"Admin",
			"User",
			"Guest"
	];
	const USER_NONE = "none@localhost";
	const DEFAULT_USER = [ 
			self::USER_NONE
	];
	const GROUP_NONE = "None";
	const DEFAULT_GOUPS = [ 
			self::GROUP_NONE
	];

	/**
	 *
	 * @Route("/install/init_roles" , name="init_roles")
	 * TODO: Response
	 */
	public function init_roles(Connection $conn) {
		$status = 'OK';
		$msg = "";
		$em = $this->getDoctrine ()->getManager ();
		$repo = $em->getRepository ( Role::class );
		foreach ( self::DEFAULT_ROLES as $r ) {
			$role = $repo->findOneBy ( [ 
					'roleName' => $r
			] );
			if (is_null ( $role )) {
				$role = new Role ();
				$role->setRoleName ( $r );
				$em->persist ( $role );
				$em->flush ();
			}
		}
		return new JsonResponse ( [ 
				'status' => $status
		] );
	}

	/**
	 *
	 * @Route("/install/init_users" , name="init_users")
	 */
	public function init_users(Connection $conn) {
		$status = 'OK';
		$msg = "";
		$em = $this->getDoctrine ()->getManager ();
		$repo = $em->getRepository ( PhpauthUsers::class );
		foreach ( self::DEFAULT_USER as $r ) {
			$user = $repo->findOneBy ( [ 
					'email' => $r
			] );
			if (is_null ( $user )) {
				$user = new PhpauthUsers ();
				$user->setEmail ( $r );
				$user->setIsactive ( 0 );
				$em->persist ( $user );
				$em->flush ();
			}
		}
		return new JsonResponse ( [ 
				'status' => $status
		] );
	}

	/**
	 *
	 * @Route("/install/init_groups" , name="init_groups")
	 */
	public function init_groups(Connection $conn) {
		$status = 'OK';
		$msg = "";
		$em = $this->getDoctrine ()->getManager ();
		$repo = $em->getRepository ( Group::class );
		foreach ( self::DEFAULT_USER as $r ) {
			$group = $repo->findOneBy ( [ 
					'name' => $r
			] );
			if (is_null ( $group )) {
				$group = new Group ();
				$group->setName ( $r );
				$epc = $this->epc;
				$group->setGroupID ( $epc->createGroupIfNotExistsFor ( $r )->groupID );
				$user = $this->getDoctrine ()->getManager ()->getRepository ( PhpauthUsers::class )->findOneBy ( [ 
						'email' => self::USER_NONE
				] );
				if (is_null ( $user ))
					$this->init_user ( $conn );
				$user = $this->getDoctrine ()->getManager ()->getRepository ( PhpauthUsers::class )->findOneBy ( [ 
						'email' => self::USER_NONE
				] );
				if (is_null ( $user )) {
					echo "critical db error.";
					die ();
				}
				$group->setOwner ( $user );
				$em->persist ( $group );
				$em->flush ();
			}
		}
		return new JsonResponse ( [ 
				'status' => $status
		] );
	}
	const PHP_DB_FILES = __DIR__ . '/../../vendor\phpauth\phpauth\database_defs/';
	const PHP_AUTH_MYSQL = self::PHP_DB_FILES . 'database_mysql.sql';
	const PHP_AUTH_PGSQL = self::PHP_DB_FILES . 'database_pgsql.sql';

	/**
	 *
	 * @Route("/install/setup/{db_type}" , name="setup")
	 */
	public function setup($db_type = null) {
		$pdo = $this->getDoctrine ()->getConnection ();
		if (! $pdo instanceof \PDO)
			return $this->redirectToRoute ( "user_wrapper" );
		switch ($db_type) {
			case 'posgresql' :
				$filename = PHP_AUTH_MYSQL;
				break;
			case 'mysql' :
			default :
				$filename = PHP_AUTH_MYSQL;
		}
		if (is_file ( $filename ) && is_readable ( $filename )) {
			$statement = file_get_contents ( $filename );
			$pdo->prepare ( $statement )->execute ( [ ] );
		} else {
			// TODO: error
		}
		return $this->render ( "setup.html.twig" );
	}

	/**
	 *
	 * @Route("/install/info" , name="phpinfo")
	 */
	public function info() {
		ob_start ();
		phpinfo ();
		$str = ob_get_contents ();
		ob_get_clean ();
		return new Response ( $str );
	}
}
