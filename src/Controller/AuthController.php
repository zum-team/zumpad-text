<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PhpauthUsers;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\PhpauthRequests;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use EtherpadLite\Client;

class AuthController extends UserWrapperController{
	public function get_register_form(PhpauthUsers $usr=null){
	    $user = is_null($usr) ? new PhpauthUsers() : $usr;
		// @formatter:off
	    return $this->createFormBuilder( $user )
        ->add( "email", EmailType::class, [
            'label'=>"E-Mail-Adresse",
        ] )
        ->add( "password", RepeatedType::class, [
                "type"=>PasswordType::class,
                "options"=>[
                    "attr"=>[
                        "class"=>'password-field'
                                    
                ] ],
                "required"=>true,
                "first_name"=>'password',
                "first_options"=>[
                    "label"=>'Passwort'
                ],
                "second_name"=>'passwordConfirm',
                "second_options"=>[
                    "label"=>'Passwort wiederholen'
        ] ] )
        ->add( "save", SubmitType::class, [
            "label"=>'Benutzerkonto anlegen',
            'attr' => ['class' => 'btn secondary'],
        ] )
        ->getForm();
        // @formatter:on
	}
	public function get_login_form(PhpauthUsers $usr=null){
	    $user = is_null($usr) ? new PhpauthUsers() : $usr;
	    // @formatter:off
	    return $this->createFormBuilder( $user )
	    ->add( "email", EmailType::class, [
	        'label'=>"E-Mail-Adresse",
	    ] )
        ->add( "password", PasswordType::class )
        ->add( "save", SubmitType::class, [
            "label"=>'Anmelden',
            'attr' => ['class' => 'btn secondary'],
        ] )
        ->getForm();
        // @formatter:on
	}
	public function get_recover_form(PhpauthUsers $usr=null){
	    $user = is_null($usr) ? new PhpauthUsers() : $usr;
	    
		// @formatter:off
	    return $this->createFormBuilder($user)
	    ->add( "email", EmailType::class, [
	        'label'=>"E-Mail-Adresse",
	    ] )
        ->add( "save", SubmitType::class, [
            "label"=>'Passwort zusenden'
        ])
        ->getForm();
        // @formatter:on
	}
	public function get_reset_form(PhpauthRequests $auth_request){
		// @formatter:off
        return $this->createFormBuilder( $auth_request )
        ->add( "token", TextType::class, [ "label"=>'Password reset key'] )
        ->add( "password", RepeatedType::class, [
            "type"=>PasswordType::class,
            "options"=>[
                "attr"=>[
                    "class"=>'password-field'
            ] ],
            "required"=>true,
            "first_name"=>'password',
            "first_options"=>[
                "label"=>'Passwort' 
            ],
            "second_name"=>'passwordConfirm',
            "second_options"=>["label"=>'Passwort zur Kontrolle' 
        ] ] )
        ->add( "save", SubmitType::class, ["label"=>'Passwort zusenden', 'attr' => ['class' => 'btn secondary']])
        ->getForm();
            // @formatter:on
	}
	
	/**
	 * @Route("/register", name="register")
	 */
	public function register(Request $request){
		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository(PhpauthUsers::class);
		$usr = new PhpauthUsers();
		
		$form = $this->get_register_form($usr);
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			$reg = $this->auth->register($usr->getEmail(), $usr->getPassword(), $usr->getPassword());
			if($reg["error"]){
				$form->addError(new FormError($reg["message"]));
			}else{
				$authorID = $this->epc->createAuthor($usr->getEmail())->authorID;
				$update_user = $repo->find($this->auth->getUID($usr->getEmail()));
				$update_user->setAuthorID($authorID);
				$em->flush();
				return $this->redirectToRoute("user_wrapper");
			}
		}
		return $this->render("register.html.twig", array_merge(["form"=>$form->createView() 
		], $this->to_array()));
	}
	
	/**
	 *
	 * @Route("/login", name="login")
	 */
	public function show_login(Request $request, Connection $conn, UrlGeneratorInterface $router){
	    $usr = new PhpauthUsers();
		// TODO: load data from cookie?
		$form = $this->get_login_form($usr);
		$data = $request->get("form");
		$data = is_array($data) ? $data : [];
		if(array_key_exists("email", $data) && array_key_exists("password", $data)){
			$result = $this->auth->login($data["email"], $data['password']);
			if($result["error"]){
				$form->addError(new FormError($result["message"]));
			}else{
				$this->auth->getUID($data["email"]);
				// check if user hast a valid authorID
				try {
			        $em = $this->getDoctrine()->getManager();
			        $repo = $em->getRepository(PhpauthUsers::class);
			        $usr = $repo->findOneBy(['email'=>$data["email"]]);
			        $this->get_epc()->listPadsOfAuthor($usr->getAuthorID());
				} catch (\Exception $e) {
				    // if authorID not exsists create one
				    if($e->getMessage() == 'authorID does not exist') {
				        $epc = $this->get_epc();
				        if(!$usr instanceof PhpauthUsers) return false;
				        if(!$epc instanceof Client) return false;
				        $usr->setAuthorID($epc->createAuthor($usr->getEmail())->authorID);
				        $em->persist($usr);
				        $em->flush();
				    }
				}
				return $this->redirectToRoute("user_wrapper");
			}
		}
		return $this->render("login.html.twig", array_merge(['form'=>$form->createView() 
		], $this->to_array()));
	}
	
	/**
	 *
	 * @Route("/recover", name="recover")
	 */
	public function show_recover_password(Request $request){
		$usr = new PhpauthUsers();
		$form = $this->get_recover_form($usr);
		$data = $request->get("form") ? $request->get("form") : [];
		if(array_key_exists("email", $data)){
			$email = $data['email'];
			$result = $this->auth->requestReset($email, true);
			if($result["error"]){
				$form->addError(new FormError($result["message"]));
			}else{
				return $this->redirectToRoute("reset");
			}
		}
		
		return $this->render("recover.html.twig", array_merge(['recover_form'=>$form->createView() 
		], $this->to_array()));
	}
	
	/**
	 *
	 * @Route("/reset", name="reset")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function show_reset_password(Request $request){
		$result = null;
		$auth_request = new PhpauthRequests();
		$form = $this->get_reset_form($auth_request);
		$data = $request->get("form") ? $request->get("form") : [];
		if(array_key_exists("token", $data) && array_key_exists("password", $data)){
			$form->handleRequest($request);
			$result = $this->auth->resetPass($auth_request->getToken(), $auth_request->getPassword(), $auth_request->getPassword());
			if($result['error']){
				$form->addError(new FormError($result["message"]));
			}
		}
		
		return $this->render("reset.html.twig", array_merge(['reset_form'=>$form->createView() 
		], $this->to_array()));
	}
	
	/**
	 *
	 * @Route("/logout", name="logout")
	 */
	public function show_logout(){
		$this->auth->logout($this->auth->getCurrentSessionHash());
		return $this->redirectToRoute("user_wrapper", $this->to_array());
	}
	
	/**
	 *
	 * @Route("/details", name="details")
	 */
	public function show_user_details(){
		return $this->render("user.html.twig", $this->to_array());
	}
	
	/**
	 *
	 * @Route("/list_all_user", name="list_all_user")
	 */
	public function show_user_list(){
		return $this->render("showusers.html.twig", $this->to_array());
	}
}
