/**
 * 
 */
function password_field_changed(e) {
    if($('#form_isPasswordProtected').val() == "1") {
	$('#form_password_password').closest('.form-group').show();
	$('#form_password_passwordConfirm').closest('.form-group').show();
    } else {
	$('#form_password_password').closest('.form-group').hide();
	$('#form_password_passwordConfirm').closest('.form-group').hide();
    }
}

$(document).ready(function() {
    $('#form_isPasswordProtected').on('change', password_field_changed);
    $('#form_isPasswordProtected').change();
    $('#form_private').closest('.form-group').hide();
    $('#form_isPasswordProtected').selectpicker({});
});
