/**
 * 
 */
function set_private(e) {
    if ($(e.target).prop("checked")) {
	$('#form_isPasswordProtected').prop("disabled", false);
	$('#form_isPasswordProtected').closest('.form-group').show();
    } else {
	$('#form_isPasswordProtected').val(0);
	$('#form_isPasswordProtected').prop("disabled", true);
	$('#form_isPasswordProtected').closest('.form-group').hide();
    }

    $('#form_isPasswordProtected').change();
}

function set_passwordPretected(e) {
    // set disable to pw fields
    $('#form_password_password').prop("disabled", $(e.target).val() == 0);
    $('#form_password_passwordConfirm').prop("disabled", $(e.target).val() == 0);
    // set required to pw fields
    $('#form_password_password').prop("required", $(e.target).val() == 1);
    $('#form_password_passwordConfirm').prop("required", $(e.target).val() == 1);
    // hide pw fields
    if ($(e.target).val() == 1) {
	$('#form_password_password').closest('.form-group').show();
	$('#form_password_passwordConfirm').closest('.form-group').show();
    } else {
	$('#form_password_password').closest('.form-group').hide();
	$('#form_password_passwordConfirm').closest('.form-group').hide();
    }
}

$(document).ready(function() {
    $('#form_private').prop("disabled", !$('#user_wrapper').data().logged_in);

    $('#form_private').on('change', set_private);
    $('#form_private').prop('checked', $('#user_wrapper').data('logged_in')=="1");
    $('#form_isPasswordProtected').on('change', set_passwordPretected);
    // secure the pw fields in correct state
    $('#form_private').change();
    $('#form_isPasswordProtected').change();
});
