/**
 * 
 */
$(document).ready(function() {
    $('#show_pads_table').dataTable({
	ajax : {
	    'url' : '/list_pads',
	    'method' : 'POST',
	    'dataSrc' : /* 'data', */function(json) {
		$('[data-toggle="tooltip"]').tooltip();
		return json.data
	    }
	},
	'columns' : [ {
	    'data' : 'padId',
	    'visible' : false,
	    'title' : 'ID'
	}, {
	    'data' : 'title',
	    'title' : 'Pad'
	}, {
	    'data' : 'authors',
	    'title' : 'Bearbeiter',
	    'visible' : false,
	}, {
	    'data' : 'lastEdited',
	    'title' : 'Letzte Bearbeitung',
	    'render' : function(data, type, row) {
		var date = new Date(data);
		return date.toLocaleString('de-DE');
	    },
	}, {
	    'data' : 'isPublic',
	    'title' : 'Öffentlich',
	    'render' : function(data, type, row) {
		switch (type) {
		    case 'display':
			var tool_tip = row.isPublic ? "Öffentlich" : "Privat";
			return '<span data-toggle="tooltip" title="' + tool_tip + '" class="oi oi-' + (row.isPublic ? 'globe' : 'key') + '"></span>';
		    default:
			return data;
		}
		return data;
	    }
	// 'visible' : false,
	}, {
	    'data' : 'isPasswordProtected',
	    'title' : 'Passwortgeschützt',
	    'render' : function(data, type, row) {
		switch (type) {
		    case 'display':
			var tool_tip = row.isPasswordProtected ? "Geschützt" : "Ungeschützt";
			return '<span data-toggle="tooltip" title="' + tool_tip + '" class="oi oi-lock-' + (row.isPasswordProtected ? 'locked' : 'unlocked') + '"></span>';
		    default:
			return data;
		}
		return data;
	    }
	}, {
	    'data' : 'isReadOnly',
	    'title' : 'Nur lesbar',
	    'visible' : false,
	}, {
	    'data' : 'revisions',
	    'title' : 'Version',
	    'visible' : false,
	}, {
	    // 'data' : 'misc',
	    'render' : function(data, type, row) {
		switch (type) {
		    case 'display':
			var action_div = $('<div></div>');
			var wrench_btn = $('<button data-padID="' + row.padId + '" type="button" data-toggle="tooltip" name="pad_options" class="btn btn-default"><span class="oi oi-wrench"></span></button>');
			var trash_btn = $('<button data-padID="' + row.padId + '" type="button" data-toggle="tooltip" name="delete_pad" class="btn btn-default"><span class="oi oi-trash"></span></button>');
			action_div.data(data);
			if (row.show_options !== true)
			    wrench_btn.prop("disabled", true);
			if (row.is_deleteable !== true)
			    trash_btn.prop("disabled", true);
			action_div.append(wrench_btn).append("&nbsp;").append(trash_btn);
			return action_div.html();
		    default:
			return "";
		}
	    },
	    // visible : false,
	    'title' : ""
	}, ],
    // 'columnDefs' : [{}]
    });
    $('#show_pads_table tbody').on('click', 'button[name="pad_options"]', function(e) {
	var padID = $(e.target).closest('button').attr("data-padID");
	$.ajax({
	    url : "/edit_pad/" + padID,
	    method : 'POST',
	    data : {},
	    success : function(response) {
		$('#insert_edit_modal').empty().append(response);
		$('#pad_modal').modal('show');
	    },
	});
    });

    $('#show_pads_table tbody').on('click', 'button[name="delete_pad"]', function(e) {
	var padID = $(e.target).closest('button').attr("data-padID");
	var row = $(e.target).parents('tr');
	$.ajax({
	    url : "/delete_pad/" + padID,
	    method : 'POST',
	    data : {},
	    success : function(response) {
		$('#insert_edit_modal').empty().append(response);
		$('#pad_modal').modal('show');
		if (response.status == "OK") {
		    row.remove().draw();
		} else {
		    alert(response.msg);
		}
	    },
	});
    });
});
