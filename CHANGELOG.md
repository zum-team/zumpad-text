# Changelog
Wichtige Änderungen werden hier gesammelt.


Das Format lehnt sich an [Keep a Changelog](https://keepachangelog.com/de/1.0.0/) an.

## [Unreleased](https://gitlab.com/zum-team/zumpad-text/-/compare/v0.2...master)

## [v0.2](https://gitlab.com/zum-team/zumpad-text/-/releases/v0.2) - 2020-09-15
### Added
- DEV: Docker Setup für lokale Entwicklung

### Changed
- Startseite aufgeräumt
- Seitenfußbereich in Menü umgezogen
- Padseite im Vollbildstil
- DEV: Templatestruktur umgearbeitet
- DEV: Doctrine initialisierung per symfony/orm-pack
- DEV: `getenv` durch Symfony Parameter ersetzt

### Fixed
- Rechtschreibfehler

### Removed
- "Gruppen" Funktionalität entfernt
- "Admin" Funktionsrumpf entfernt